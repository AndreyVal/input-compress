
import './App.css';
import UploadFile from "./components/UploadFile";

function App() {
  return (
    <div className="box">
        <h2 className="header">Compression file</h2>
        <UploadFile />
    </div>
  );
}

export default App;
