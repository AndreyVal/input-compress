import "./uploadFile.css"

import React, {useRef, useState} from 'react';
import fileDownload from 'js-file-download';
import axios from "axios";
import {SlCloudUpload} from 'react-icons/sl'
import {ImageConfig} from "../ImageConfig";

const UploadFile = () => {
    const [file, setFile] = useState([])

    const wrapperRef = useRef(null);

    const onDragEnter = () => wrapperRef.current.classList.add('dragover');

    const onDragLeave = () => wrapperRef.current.classList.remove('dragover');

    const onDrop = () => wrapperRef.current.classList.remove('dragover');



    const onFileDrop = (e) => {
        const newFile = e.target.files[0];
        if (newFile) {
            setFile([newFile]);
        }
    }

    const fileRemove = () => setFile([]);

    const handleUploadFile = () => {
        const formData = new FormData()
         formData.append("video", file[0])
        axios.post("http://localhost:3000/compress", formData, {
            headers:{
                "Content-Type": "multipart/form-data",
            },
            responseType: "blob"
        })
            .then((res) => {
                const fileName = file[0].name
                fileDownload(res.data, fileName)
            })
            .catch((err) => console.log(err))
    }




    return (
        <>
            <div
                ref={wrapperRef}
                onDragEnter={onDragEnter}
                onDragLeave={onDragLeave}
                onDrop={onDrop}
                className='upload'
            >
                <div className="upload__file-label">
                    <SlCloudUpload className='upload__icon'/>
                    <p className='upload__desc'>Drag & Drop your files here</p>
                </div>
                <input accept={"video/*, .mp4, .mov, .avi, .wmv,"} onChange={onFileDrop} className='upload__input' type="file"/>
            </div>

            {
                file.length > 0 ? (
                    <div className='drop__file-preview'>
                        <p className='drop__file-preview-title'>
                           video is ready to be compressed
                        </p>
                        {
                            file.map((item, index) => (
                                <div key={index} className='drop__file-preview-item'>
                                    <img className='image' src={ImageConfig[item.type.split('/')
                                        [1]] || ImageConfig['default']} alt=""/>
                                    <div className='drop__file-preview-item-info'>
                                        <p>{item.name}</p>
                                        <p>{Math.floor(item.size / 1000000)}MB</p>
                                    </div>
                                    <span onClick={fileRemove} className='drop__file-preview-item-del'>x</span>
                                </div>
                            ))
                        }
                        {
                            <button className='drop__file-btn' type="submit" onClick={handleUploadFile}>Start compress</button>
                        }
                    </div>
                ) : null
            }
        </>
    );
};
export default UploadFile;