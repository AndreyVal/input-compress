import fileMp4 from './assets/mp4.png';
import fileMov from './assets/mov.svg';
import fileAvi from './assets/avi.png';
import fileDefault from './assets/default-file.svg';

export const ImageConfig = {
    mp4: fileMp4,
    mov: fileMov,
    avi: fileAvi,
    default: fileDefault
}